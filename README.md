# CovyBot - Bot Service

Sample bot service backend illustrating various menu options and survey flow as a prototype for implementing Covybot on the WhatsApp Business API.

## Running

Run script with `node covybot` to access the bot menu.  Reply to the bot by appending the `--reply` argument as below:

`node covybot --reply=1` or `node covybot --reply="Christ Church"`

