// define bot menu
const bot_menu = `
Sample Bot Menu
=====
Please respond with a number to continue:

1. Latest local stats
2. Latest global stats
3. How To Protect Yourself
4. Track Your Symptoms (self-assessment)
5. Privacy Policy
`;

// define bot responses
const bot_responses = {
  1: `
  Total confirmed cases: 146,175
  Total confirmed deaths: 4,776
  Total recovered: 7,404
  `,
  2: `
  Total confirmed cases: 4,946,175
  Total confirmed deaths: 324,776
  Total recovered: 1,717,404
  `,
  3: `Here are some great tips on how to protect yourself from COVID19:`,
  5: `Privacy Policy attached: ...`,
};

const survey_questions = [
  `Use this self-assessment tool to help determine whether you need to be tested for COVID-19. You can complete this assessment for yourself or on behalf of someone else, if they are not able.
  
  (1) What is your age?`,
  `(2) What is your gender?`,
  `(3) What parish do you reside in?`,
];

module.exports = {
  bot_menu,
  bot_responses,
  survey_questions,
};
