const args = process.argv;
const { runSurvey, reply } = require("./services/bot");
const redis = require("./services/redis");

// set constants
const phone = "12468201742";

/**
 * main execution
 */
const run = async () => {
  // get response from command args
  const response = args[2]?.split("=")[1];

  // get session from redis
  const userHasSession = await redis.get(phone);

  // detect if user is in survey or requesting static info
  if (userHasSession || response === "4") {
    // run survey 
    await runSurvey(phone, response);
  } else {
    // get bot reply 
    await reply(response);
  }
};

run();
console.log("");
