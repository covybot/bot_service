const axios = require("axios");
const config = require("../config");

class ApiClient {
  constructor() {
    this.client = axios.create({ baseURL: config.api.base_url, timeout: 36000 });
  }

  /**
   * make API request
   * 
   * @param {*} method 
   * @param {*} url 
   * @param {*} params 
   * @param {*} data 
   */
  async _request(method, url, params = {}, data = {}) {
    try {
      const res = await this.client({
        method,
        url,
        params,
        data,
      });

      return { status: res.status, data: res.data };
    } catch (e) {
      throw new Error(e);
    }
  }
}

module.exports = new ApiClient();