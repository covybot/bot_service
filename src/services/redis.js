const redis = require("redis");
const client = redis.createClient();

module.exports = {
  get: async (key) => {
    return new Promise((resolve, reject) => {
      client.get(key, (err, reply) => {
        if (err) reject(err);

        resolve(reply);
      });
    });
  },
  set: async (key, value) => {
    return new Promise((resolve, reject) => {
      client.set(key, value, (err, reply) => {
        if (err) reject(err);

        resolve(reply);
      });
    });
  },
  del: async (key) => {
    return new Promise((resolve, reject) => {
      client.del(key, (err, reply) => {
        if (err) reject(err);

        resolve(reply);
      });
    });
  },
};
