module.exports = (message) => {
  const timestamp = new Date().toISOString();
  const service_name = "covybot";

  console.log(`${timestamp} ${service_name} says: \n` + message + '\n');
};
