const redis = require("./redis");
const log = require("./log");
const { bot_menu, bot_responses, survey_questions } = require("../menus");

module.exports = {
  /**
   * reply to bot message by menu ID
   */
  reply: async (reply) => {
    /**
     * if no message sent, or 0 sent
     * reply with bot menu
     */
    if (!reply || reply === "0") {
      log(bot_menu);
      process.exit(0);
    } else {
      /**
       * send message by menu item recieved
       */
      log(bot_responses[reply] || 'Menu option not available. Try again.');
      process.exit(0);
    }
  },
  /**
   * run interactive survey routine
   */
  runSurvey: async (phone, reply) => {
    const userSession = await redis.get(phone);

    /**
     * if session is active and reply was received
     * @todo: record response to py backend
     * set next_step in redis
     */
    if (userSession && reply && reply !== "0") {
      const next_step = +userSession + 1;
      await redis.set(phone, next_step);
    }

    /**
     * check if user has an active session
     */
    if (userSession) {
      const current_step = await redis.get(phone);

      /**
       * check if current_step is last step
       * if so, exit routine
       */
      if (current_step >= survey_questions.length) {
        log("Thank you for participating.");
        await redis.del(phone);
        process.exit(0);
      }

      /**
       * show next question in survey
       */
      log(survey_questions[current_step]);
      process.exit(0);
    } else {
      await redis.set(phone, 0);
      log(survey_questions[0]);
      process.exit(0);
    }
  },
};
